package Task3;

import java.io.IOException;
import java.util.ArrayList;

public class Runner {
    public static void main(String args[])
            throws IOException {
        try{
            ArrayList<MyStruct2> list = MyStruct2.Scan(args[0]);
            System.out.println("Readed data: ");
            MyStruct2.Prints(list);
            MyStruct2.SortByDate(list);
            System.out.println("\nSorted by Date: ");
            MyStruct2.Prints(list);
            MyStruct2.SortByCard(list);
            MyStruct2.PrintTotal(list);
        } catch (Exception e){
            System.out.println("Something wrong: "+e);
        }
    }
}
