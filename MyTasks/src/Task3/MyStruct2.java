package Task3;

import Task2.MyStruct;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyStruct2 {
    int CardNumber;
    String ShopName;
    String PurchaseName;
    int Date;
    int Cost;
    double Discount;

    public void DiscountCalc(){
        Discount = (double) Cost*5/100;
    }

    public String toString() {
        return "MyStruct2{" +
                "CardNumber='" + CardNumber + '\'' +
                ", ShopName='" + ShopName + '\'' +
                ", PurchaseName='" + PurchaseName + '\'' +
                ", Date='" + Date + '\'' +
                ", Cost='" + Cost + '\'' +
                ", Discount='" + Discount + '\'' +
                '}';
    }

    public static ArrayList<MyStruct2> Scan(String f)
            throws IOException {
        String s[];
        ArrayList<MyStruct2> al = new ArrayList<MyStruct2>();
        Scanner scanner = new Scanner(new File(f));
        while (scanner.hasNext()) {
            s = scanner.nextLine().split(";");
            MyStruct2 ms = new MyStruct2();
            ms.CardNumber = Integer.parseInt(s[0]);
            ms.ShopName = s[1];
            ms.PurchaseName = s[2];
            ms.Date = Integer.parseInt(s[3]);
            ms.Cost = Integer.parseInt(s[4]);
            ms.DiscountCalc();
            al.add(ms);
        }
        scanner.close();
        return al;
    }

    public static void SortByDate(ArrayList<MyStruct2> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).Date < list.get(j).Date){
                    MyStruct2 a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }

    public static void SortByCard(ArrayList<MyStruct2> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).CardNumber < list.get(j).CardNumber){
                    MyStruct2 a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }

    public static void Prints(ArrayList<MyStruct2> list)
    {
        System.out.println("|----------|----------------|----------------|------|------|--------------|" );
        System.out.println("|CardNumber|    ShopName    |  PurchaseName  | Date | Cost |   Discount   |");
        System.out.println("|----------|----------------|----------------|------|------|--------------|");
        for (MyStruct2 ms : list)
        {
            System.out.printf("|%10d|%16s|%16s|%6d|%6d|%14f|\n",ms.CardNumber,ms.ShopName,ms.PurchaseName,
                    ms.Date,ms.Cost,ms.Discount);
        }
        System.out.println("|----------|----------------|----------------|------|------|--------------|");
    }

    public static void PrintTotal(ArrayList<MyStruct2> list) {
        SortByCard(list);
        System.out.println("\nTotal Cost: ");
        int i = list.size() - 1;
        int c = 0;
        boolean last = false;
        while (i>0)
        {
            c += list.get(i).Cost;
            if (list.get(i).CardNumber != list.get(i-1).CardNumber)
            {
                System.out.println("CardNumber: "+ list.get(i).CardNumber+", Total: "+c);
                c = 0;
            }

            if (i == 1 && (list.get(1).CardNumber == list.get(0).CardNumber))
                c += list.get(0).Cost;
            else
                last = true;

            i--;
        }
        if (last == true)
            System.out.println("CardNumber: "+ list.get(0).CardNumber+", Total: "+list.get(0).Cost);
    }


}
