package Task2;

import java.io.IOException;
import java.util.ArrayList;

public class Runner {
    public static void main(String args[])
            throws IOException {
        try{
            ArrayList<MyStruct> list = MyStruct.Scan(args[0]);
            System.out.println("Readed data: ");
            MyStruct.Prints(list);
            MyStruct.SortByDate(list);
            System.out.println("\nSorted by Date: ");
            MyStruct.Prints(list);
            MyStruct.SortByCard(list);
            MyStruct.PrintTotal(list);
        } catch (Exception e){
            System.out.println("Something wrong: "+e);
        }
    }
}
