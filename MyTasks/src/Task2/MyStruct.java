package Task2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyStruct{
    int CardNumber;
    String ShopName;
    String PurchaseName;
    int Date;
    int Cost;

    public String toString() {
        return "MyStruct{" +
                "CardNumber='" + CardNumber + '\'' +
                ", ShopName='" + ShopName + '\'' +
                ", PurchaseName='" + PurchaseName + '\'' +
                ", Date='" + Date + '\'' +
                ", Cost='" + Cost + '\'' +
                '}';
    }

    public static ArrayList<MyStruct> Scan(String f)
            throws IOException {
        String s[];
        ArrayList<MyStruct> al = new ArrayList<MyStruct>();
        Scanner scanner = new Scanner(new File(f));
        while (scanner.hasNext()) {
            s = scanner.nextLine().split(";");
            MyStruct ms = new MyStruct();
            ms.CardNumber = Integer.parseInt(s[0]);
            ms.ShopName = s[1];
            ms.PurchaseName = s[2];
            ms.Date = Integer.parseInt(s[3]);
            ms.Cost = Integer.parseInt(s[4]);
            al.add(ms);
        }
        scanner.close();
        return al;
    }

    public static void SortByDate(ArrayList<MyStruct> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).Date < list.get(j).Date){
                    MyStruct a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }

    public static void SortByCard(ArrayList<MyStruct> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).CardNumber < list.get(j).CardNumber){
                    MyStruct a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }

    public static void PrintTotal(ArrayList<MyStruct> list) {
        SortByCard(list);
        System.out.println("\nTotal Cost: ");
        int i = list.size() - 1;
        int c = 0;
        boolean last = false;
        while (i>0)
        {
            c += list.get(i).Cost;
            if (list.get(i).CardNumber != list.get(i-1).CardNumber)
            {
                System.out.println("CardNumber: "+ list.get(i).CardNumber+", Total: "+c);
                c = 0;
            }

            if (i == 1 && (list.get(1).CardNumber == list.get(0).CardNumber))
                c += list.get(0).Cost;
            else
                last = true;

            i--;
        }
        if (last == true)
            System.out.println("CardNumber: "+ list.get(0).CardNumber+", Total: "+list.get(0).Cost);
    }

    public static void Prints(ArrayList<MyStruct> list)
    {
        System.out.println("|----------|----------------|----------------|------|------|" );
        System.out.println("|CardNumber|    ShopName    |  PurchaseName  | Date | Cost |");
        System.out.println("|----------|----------------|----------------|------|------|");
        for (MyStruct ms : list)
        {
            System.out.printf("|%10d|%16s|%16s|%6d|%6d|\n",ms.CardNumber,ms.ShopName,ms.PurchaseName,
                    ms.Date,ms.Cost);
        }
        System.out.println("|----------|----------------|----------------|------|------|");
    }
}
