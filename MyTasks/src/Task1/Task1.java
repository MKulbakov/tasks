package Task1;

import java.io.*;
import java.util.*;

public class Task1{

    public static String Scan(String f)
            throws IOException{
        String s = "";
        BufferedReader bf = new BufferedReader(new FileReader(f));
        int c = bf.read();
        while (c != -1)
        {
            if ((char)c >='0' && (char)c <='9') s += (char) c;
            c = bf.read();
        }
        bf.close();
        return s;
    }

    public static char[] Sort(String s)
    {
        char mas[] = new char[s.length()];
        for (int i = 0; i< mas.length; i++)
            mas[i] = s.charAt(i);
        Arrays.sort(mas);
        return mas;
    }

    public static void Print(char[] mas)
    {
        System.out.print("Sorted numbers: ");
        for (int i = mas.length-1; i>0; i--)
        {
            if (mas[i] != mas[i-1])
                System.out.print(mas[i]);
        }
        if (mas[0] != mas[1]) System.out.println(mas[0]);
    }

    public static void main(String args[])
            throws IOException {
        try{
            String s = Scan(args[0]);
            System.out.println("All numbers from your file: " + s);
            char mas[] = Sort(s);
            System.out.print("Sorted numbers: ");
            for (int i = mas.length-1; i>=0; i--)
                System.out.print(mas[i]);
            System.out.println();
            Print(mas);
        }
        catch (Exception e){
            System.out.println("Something wrong: "+e);
        }

    }
}
