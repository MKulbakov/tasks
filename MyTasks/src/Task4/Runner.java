package Task4;

import java.io.IOException;
import java.util.ArrayList;

public class Runner {
    public static void main(String args[])
            throws IOException {
        try{
            ArrayList<MyStruct3> list = MyStruct3.Scan(args[0]);
            System.out.println("Readed data: ");
            MyStruct3.Prints(list);
            MyStruct3.SortByDate(list);
            System.out.println("\nSorted by Date: ");
            MyStruct3.Prints(list);
            MyStruct3.SortByCard(list);
            MyStruct3.PrintTotal(list);
        } catch (Exception e){
            System.out.println("Something wrong: "+e);
        }
    }
}
