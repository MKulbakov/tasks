package Task4;

import Task2.MyStruct;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MyStruct3 {
    int CardNumber;
    String ShopName;
    String PurchaseName;
    int Date;
    int Cost;
    double Discount;
    int DiscSize;

    public void DiscountCalc(int d){
        Discount = (double) Cost*d/100;
    }

    public String toString() {
        return "MyStruct2{" +
                "CardNumber='" + CardNumber + '\'' +
                ", ShopName='" + ShopName + '\'' +
                ", PurchaseName='" + PurchaseName + '\'' +
                ", Date='" + Date + '\'' +
                ", Cost='" + Cost + '\'' +
                ", Discount='" + Discount + "("+DiscSize+"%)"+'\'' +
                '}';
    }

    public static void SetDisc(ArrayList<MyStruct3> al)
    {
        int size = al.size();
        for (int i = 0; i<size; i++)
            for (int j = 0; j<size; j++)
            {
                if (j == i) continue;
                if (al.get(i).CardNumber == al.get(j).CardNumber && al.get(i).ShopName.equals(al.get(j).ShopName))
                {
                    if ((al.get(j).Date - al.get(i).Date) > 28 && al.get(i).DiscSize != 5 && al.get(i).DiscSize != 10)
                    {
                        al.get(i).DiscountCalc(0);
                        al.get(i).DiscSize = 0;
                    }
                    else if ((al.get(j).Date - al.get(i).Date) <= 28 && (al.get(j).Date - al.get(i).Date) > 7
                            && al.get(i).DiscSize != 5)
                    {
                        al.get(i).DiscountCalc(10);
                        al.get(i).DiscSize = 10;
                    }
                    else if ((al.get(j).Date - al.get(i).Date) <= 7 && (al.get(j).Date - al.get(i).Date) >= 0)
                    {
                        al.get(i).DiscountCalc(5);
                        al.get(i).DiscSize = 5;
                        break;
                    }
                }
            }
    }

    public static ArrayList<MyStruct3> Scan(String f)
            throws IOException {
        String s[];
        ArrayList<MyStruct3> al = new ArrayList<MyStruct3>();
        Scanner scanner = new Scanner(new File(f));
        while (scanner.hasNext()) {
            s = scanner.nextLine().split(";");
            MyStruct3 ms = new MyStruct3();
            ms.CardNumber = Integer.parseInt(s[0]);
            ms.ShopName = s[1];
            ms.PurchaseName = s[2];
            ms.Date = Integer.parseInt(s[3]);
            ms.Cost = Integer.parseInt(s[4]);
            al.add(ms);
        }
        scanner.close();
        MyStruct3.SetDisc(al);
        return al;
    }

    public static void SortByDate(ArrayList<MyStruct3> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).Date < list.get(j).Date){
                    MyStruct3 a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }

    public static void SortByCard(ArrayList<MyStruct3> list) {
        for (int i = 0; i<list.size(); i++)
            for (int j = list.size()-1; j > i; j--){
                if (list.get(j-1).CardNumber < list.get(j).CardNumber){
                    MyStruct3 a = list.get(j-1);
                    list.remove(j-1);
                    list.add(j,a);
                }
            }
    }


    public static void PrintTotal(ArrayList<MyStruct3> list) {
        SortByCard(list);
        System.out.println("\nTotal Cost: ");
        int i = list.size() - 1;
        int c = 0;
        boolean last = false;
        while (i>0)
        {
            c += list.get(i).Cost;
            if (list.get(i).CardNumber != list.get(i-1).CardNumber)
            {
                System.out.println("CardNumber: "+ list.get(i).CardNumber+", Total: "+c);
                c = 0;
            }
            if (i == 1 && (list.get(1).CardNumber == list.get(0).CardNumber))
                c += list.get(0).Cost;
            else
                last = true;
            i--;
        }
        if (last == true)
            System.out.println("CardNumber: "+ list.get(0).CardNumber+", Total: "+list.get(0).Cost);
    }

    public static void Prints(ArrayList<MyStruct3> list)
    {
        System.out.println("|----------|----------------|----------------|------|------|--------------|" );
        System.out.println("|CardNumber|    ShopName    |  PurchaseName  | Date | Cost |   Discount   |");
        System.out.println("|----------|----------------|----------------|------|------|--------------|");
        for (MyStruct3 ms : list)
        {
            System.out.printf("|%10d|%16s|%16s|%6d|%6d|%9f(%2d%%)|\n",ms.CardNumber,ms.ShopName,ms.PurchaseName,
                    ms.Date,ms.Cost,ms.Discount,ms.DiscSize);
        }
        System.out.println("|----------|----------------|----------------|------|------|--------------|");
    }
}
